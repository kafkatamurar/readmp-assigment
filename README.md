Assignment Specification
====

Please note that I'm not sure if the program works on Linux, as it did not work on WSL. However, it works on Windows

## **Installation**

Before running the program, make sure you have all the necessary packages installed by running the following command:

`pip install -r requirements.txt`

---

## **Usage**

To run the program, use the following command:

`python3 src/main.py -h`

This will show the help message, which provides information about the available command-line arguments. You can then use the appropriate arguments to download IOC data from a source, connect to a database, or both.

---

## **Task**

Your task is to implement a command line application that will process and store
malicious IOCs1 from all specified data sources on the Internet into a database.

### **Technical requirements** (bold text) and recommendations

- Programming language: **Python 3** (ideally in Python 3.7 or higher).
- Database: **any relational database** (ideally PostgreSQL).
- Some unit and integration tests are nice to have, but not required.

### **Database** schema is not strictly defined, but

- There must be a separate table for storing IP addresses and another
table for storing URLs.
Let’s consider <http://178.62.47.209/banks/ATB/last.html> to be URL
and not IP address.
- One line in a given data source (for example one URL) must be stored
as one row in a database table.
Consider a hypothetical use case, that each URL and IP address must
be unique and easily searchable. You cannot store the whole input from
a given data source into one database record.
- There must be a table containing origin of a given data.
For example, if URL <http://domain.com> was extracted from OpenPhish, then this URL must be associated with OpenPhish somehow.

### Data sources

- ~~<https://www.badips.com/get/list/any/2>~~
- <http://reputation.alienvault.com/reputation.data>
- <https://openphish.com/feed.txt>
- <https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/firehol_level1.netset>

Everything else depends on your imagination. Good luck, and most importantly, have fun!
