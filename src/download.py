import requests

# process the data from the link


def download_ioc_data(link: str) -> str:
    try:
        response = requests.get(link)
        if response.status_code == 200:
            return response.text
        else:
            print(
                f"Failed to download data from {link}, status code: {response.status_code}")
            return None
    except Exception as e:
        print(f"Failed to download data from {link}, error: {e}")
        return None
