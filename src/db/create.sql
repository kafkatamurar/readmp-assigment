---- TABLES ----
CREATE TABLE IF NOT EXISTS sources
(
    id_source bigserial PRIMARY KEY,
    source    text NOT NULL
);

CREATE TABLE IF NOT EXISTS urls
(
    id_url      bigserial PRIMARY KEY,
    url         text NOT NULL
);

CREATE TABLE IF NOT EXISTS ip_addresses
(
    id_ip_address bigserial PRIMARY KEY,
    ip_address    text NOT NULL
);

CREATE TABLE IF NOT EXISTS source_ip
(
    id_source bigint NOT NULL,
    id_ip_address bigint NOT NULL,
    PRIMARY KEY (id_source, id_ip_address),
    FOREIGN KEY (id_source) REFERENCES sources(id_source),
    FOREIGN KEY (id_ip_address) REFERENCES ip_addresses(id_ip_address)
);
CREATE TABLE IF NOT EXISTS source_urls(
    id_source bigint NOT NULL,
    id_url bigint NOT NULL,
    PRIMARY KEY(id_source, id_url),
    FOREIGN KEY(id_source) REFERENCES sources(id_source),
    FOREIGN KEY(id_url) REFERENCES urls(id_url)
);
---- INDEXES ----
CREATE INDEX IF NOT EXISTS id_source_index ON sources (id_source);

---- TRIGGERS AND FUNCTIONS ----

-- fill source_ip table
CREATE OR REPLACE FUNCTION fill_source_ip()
    RETURNS trigger AS
$$
BEGIN
    INSERT INTO source_ip(id_source, id_ip_address)
    VALUES ((SELECT MAX(id_source) FROM sources), NEW.id_ip_address);
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER insert_source_ip
    AFTER INSERT
    ON ip_addresses
    FOR EACH ROW
EXECUTE FUNCTION fill_source_ip();

-- fill source_urls table
CREATE OR REPLACE FUNCTION fill_source_url()
    RETURNS trigger AS
$$
BEGIN
    INSERT INTO source_urls(id_source, id_url)
    VALUES ((SELECT MAX(id_source) FROM sources), NEW.id_url);
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER insert_source_url
    AFTER INSERT
    ON urls
    FOR EACH ROW
EXECUTE FUNCTION fill_source_url();