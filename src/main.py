import argparse
from parse_by_type import *
from download import *
from database import *


def main():

    # arguement parser
    parser = argparse.ArgumentParser(
        'Download IOC data from a source.')
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-c', '--connect', action='store_true',
                        help='connect to the database. use this flag in case of a database connection error and follow further instructions.')
    parser.add_argument('-s', metavar=' ', type=str, nargs='+',
                        required=True, help='the source URLs to download the data from.')

    # parse the arguments
    args = parser.parse_args()

    # berlička
    db_creation_flag = True

    # connect to the database
    if args.connect:
        conn, db_creation_flag = db_prepare()
    else:
        conn = connect()

    if db_creation_flag == False:
        print("Failed to create the tables")
        return

    # download the data from the source
    for link in args.s:
        data = download_ioc_data(link)

        if data is None:
            print("Failed to download data")
            return

        # split the data into lines
        data_list = data.splitlines()

        # parse the data into urls and ip addresses
        urls, ip_addresses = parse_data(data_list)

        # insert the source into the database
        if urls or ip_addresses:
            insert_source(conn, link)

        # insert the urls into the database
        if urls:
            insert_url(conn, urls)

        # insert the ip addresses into the database
        if ip_addresses:
            insert_ip(conn, ip_addresses)

        # close the connection
    conn.close()


if __name__ == '__main__':
    main()
