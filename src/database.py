import psycopg2

# connect to the local database


def connect_local(host, port, database, user, password):
    try:
        conn = psycopg2.connect(
            host=host,
            port=port,
            database=database,
            user=user,
            password=password
        )
        return conn

    except Exception as e:
        print("Error: ", e)

# create the tables in the local database


def create_tables(conn):
    try:
        with open('src\db\create.sql', 'r') as f:
            db_create = f.read()

            cur = conn.cursor()
            cur.execute(db_create)
            conn.commit()
            cur.close()
    except Exception as e:
        print("Error: ", e)
        return False


def db_prepare():
    # print the instructions
    print('''
If there is an error connecting to my server (which is quite likely to happen),
you will need to set up the database yourself.
Create a database connection and follow the instructions below:
    ''')

    # input the database connection details
    host = input('Enter the host: ')
    port = input('Enter the port: ')
    database = input('Enter the database name: ')
    user = input('Enter the username: ')
    password = input('Enter the password: ')

    # connect to the database and create the tables
    conn = connect_local(host, port, database, user, password)
    fail_flag = create_tables(conn)
    return conn, fail_flag

# Connect to the database


def connect():
    try:
        conn = psycopg2.connect(
            host="172.17.0.1",
            port="5432",
            database="postgres",
            user="postgres",
            password="9266"
        )
        return conn

    except Exception as e:
        print("Error: ", e)

# Insert the ip adresses into the database


def insert_ip(conn, data):
    try:
        cur = conn.cursor()
        for item in data:
            cur.execute(
                "INSERT INTO ip_addresses (ip_address) VALUES (%s)", (item,))
        conn.commit()
        print("Data inserted successfully!")
    except Exception as e:
        conn.rollback()
        print("Error while inserting data: ", e)
    finally:
        cur.close()

# Insert the urls into the database


def insert_url(conn, data):
    try:
        cur = conn.cursor()
        for item in data:
            cur.execute(
                "INSERT INTO urls (url) VALUES (%s)", (item,))
        conn.commit()
        print("Data inserted successfully!")
    except Exception as e:
        conn.rollback()
        print("Error while inserting data: ", e)
    finally:
        cur.close()

# Insert the source into the database


def insert_source(conn, data):
    try:
        cur = conn.cursor()
        cur.execute(
            "INSERT INTO sources (source) VALUES (%s)", (data,))
        conn.commit()
    except Exception as e:
        conn.rollback()
        print("Error while inserting data: ", e)
    finally:
        cur.close()
