import ipaddress
import re
from urllib.parse import urlparse

# check if the source is an ip address


def is_ip_address(data_item):
    try:
        result = re.split(r'[^\w.]', data_item)
        ipaddress.ip_address(result[0])
        return True
    except ValueError:
        return False

# check if the source is a url


def is_url(data_item):
    try:
        result = urlparse(data_item)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False

# parse the sources into urls and ip addresses


def parse_data(data):
    urls = []
    ip_addresses = []
    for data_item in data:
        if is_ip_address(data_item):
            ip_addresses.append(data_item)
        elif is_url(data_item):
            urls.append(data_item)
    return urls, ip_addresses
